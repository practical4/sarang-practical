var express = require('express');
var router = express.Router();
var category = require('../controller/category');
var product = require('../controller/product');

const multer = require('multer');
const path = require('path');
const fs = require('fs');
   
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'public/images');
    },
   
    filename: function(req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

var upload = multer({ storage: storage })


router.post('/category/add', function(req, res, next) {
	let params = {
		name: req.body.name,
		type: req.body.type
	};
	var callback = function(data){
		 res.send(data);
	}
	category.addCategory(params, callback);
});

router.post('/category/:id', function(req, res, next) {
	let params = {
		name: req.body.name,
		type: req.body.type
	};
	var callback = function(data){
		 res.send(data);
	}
	category.updateCategory(params, req.params.id, callback);
});

router.delete('/category/:id', function(req, res, next) {
	var callback = function(data){
	  res.send(data);
  }
  category.deleteCategory(req.params.id, callback)
});

// product

router.post('/product/add', upload.single('product_image'), function(req, res, next) {
	var callback = function(data){
		res.send(data);
   	};
	if(req.file) {
		let params = {
			name: req.body.name,
			sku: req.body.sku,
			category_id: req.body.category_id,
			price: req.body.price,
			image: req.file.path
		};
		product.addProduct(params, callback);
	} else {
		callback({result:"fail", msg:'image not found'})
	}
});

router.post('/product/:id', upload.single('product_image'), function(req, res, next) {
	var callback = function(data){
		res.send(data);
   	};
	let params = {
		name: req.body.name,
		sku: req.body.sku,
		category_id: req.body.category_id,
		price: req.body.price
	};
	if(req.file) {
		params['image'] = req.file.path;
	}
	product.updateProduct(params, req.params.id, callback);
});

router.delete('/product/:id', function(req, res, next) {
	var callback = function(data){
	  res.send(data);
  }
  product.deleteProduct(req.params.id, callback)
});

module.exports = router;
